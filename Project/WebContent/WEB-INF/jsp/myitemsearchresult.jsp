<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト ホーム画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/myitemsearchresult.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- 検索フォーム -->
	<jsp:include page="/baselayout/form.jsp" />

	<div class="row center">
		<h3>検索結果</h3>
		<p>検索結果${itemCount}件</p>
	</div>
	<div class="section">
		<!--   商品情報   -->
		<div class="row">
			<c:forEach var="item" items="${itemList}">
				<div class="col">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="Item?item_id=${item.id}"><img
									src="img/${item.fileName}" width="300px" height="200px"></a>
							</div>
							<div class="col">
								<p class="font-weight-bold">${item.name}</p>
								<p>${item.formatPrice}円</p>
							</div>
						</div>
					</div>
				</div>
				<c:if test="${(status.index + 1) % 4 == 0}">
		</div>
		<div class="row">
			</c:if>
			</c:forEach>
		</div>

		<!-- test属性がtuueになった場合の処理 -->
		<!-- <li>タグはリストの項目を表示させる -->
		<!-- elseと同じ処理 -->
		<!-- class=disabledは要素を無効化する -->

		<div class="row center">
			<!-- １ページ戻るボタン  -->
			<c:choose>
				<c:when test="${pageNum == 1 }">
					<li class=disabled><a><img src="img/inequality_sign-2.png"
							width="20px" height="20px"></a></li>
				</c:when>

				<c:otherwise>
					<li class="waves-effect"><a
						href="MyItemSearchResult?search_category=全てのカテゴリー&search_word=${searchWord}&page_num=${pageNum - 1}"><img
							src="img/inequality_sign-2.png" width="20px" height="20px"></a></li>
				</c:otherwise>
			</c:choose>

			<!-- ページインデックス -->
			<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}"
				end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1"
				varStatus="status">
				<li <c:if test="${pageNum == status.index }"> class="active" </c:if>><a
					href="MyItemSearchResult?search_category=全てのカテゴリー&search_word=${searchWord}&page_num=${status.index}"
					class="text-white bg-dark">${status.index}</a></li>
			</c:forEach>

			<!-- １ページ進むボタン  -->
			<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0 }">
					<li class=disabled><a><img src="img/inequality_sign-1.png"
							width="20px" height="20px"></a></li>
				</c:when>
				<c:otherwise>
					<li class="waves-effect"><a
						href="MyItemSearchResult?search_category=全てのカテゴリー&search_word=${searchWord}&page_num=${pageNum + 1}"><img
							src="img/inequality_sign-1.png" width="auto" height="20px"></a></li>
				</c:otherwise>
			</c:choose>
		</div>
</body>
</html>