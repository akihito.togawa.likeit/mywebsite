<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト 新規ユーザー登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<div class="full-width" id="header">
		<div class="row justify-content-md-center">
			<div id="title" class="col">防音材ECサイト</div>
		</div>
	</div>
	<div class="mx-auto" style="width: 1000px;">
		<h5>新規登録</h5>
		<c:if test="${validationMessage != null}">
			<P class="red-text">${validationMessage}</P>
		</c:if>

		<form action="MyRegistConfirm" method="post">
			<!-- ユーザー名 -->
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">ユーザー名</label>
				<div class="col-sm-6">
					<input name="user_name" type="text" class="form-control"
						id="inputEmail3" placeholder="ユーザー名">
				</div>
			</div>

			<!-- ログインID -->
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-6">
					<input name="login_id" type="text" class="form-control"
						id="inputEmail3" placeholder="ログインID">
				</div>
			</div>

			<!-- 住所 -->
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">住所</label>
				<div class="col-sm-6">
					<input name="address" type="text" class="form-control"
						id="inputEmail3" placeholder="住所">
				</div>
			</div>

			<!-- パスワード -->
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-6">
					<input name="password" type="password" class="form-control"
						id="inputEmail3" placeholder="パスワード">
				</div>
			</div>

			<!-- パスワード確認用 -->
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">パスワード確認用</label>
				<div class="col-sm-6">
					<input name="confirm_password" type="password" class="form-control"
						id="inputEmail3" placeholder="パスワード確認用">
				</div>
			</div>

			<button type="submit" class="btn btn-primary mb-2">確認</button>
		</form>
	</div>
</body>
</html>