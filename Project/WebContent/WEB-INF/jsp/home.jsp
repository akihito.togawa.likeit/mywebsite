<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト ホーム画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<h1 class="row justify-content-md-center m-3">SHIZUKA Inc.</h1>
	<h3 class="row justify-content-md-center">オンラインショップ</h3>


	<!-- 検索フォーム -->
	<jsp:include page="/baselayout/form.jsp" />

	<div class="m-5">
	</div>

	<!-- 商品一覧 -->
	<div class="container" id="item">
		<div class="row" id="itembox">
			<c:forEach var="item" items="${itemList}">
				<div class="col" id="itembox">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="Item?item_id=${item.id}"><img
									src="img/${item.fileName}" width="200px" height="150px"></a>
							</div>
							<div class="col">
								<p class="font-weight-bold">${item.name}</p>
								<p>${item.formatPrice}円</p>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</body>

</html>