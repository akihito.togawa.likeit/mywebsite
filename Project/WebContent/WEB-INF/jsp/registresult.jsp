<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト 新規ユーザー登録</title>

<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<div class="full-width" id="header">
		<div class="row justify-content-md-center">
			<div id="title" class="col">防音材ECサイト</div>
		</div>
	</div>

	<h5>登録完了</h5>
	<P class="red-text">${validationMessage}</P>

	<!-- ユーザー名 -->
	<div class="form-group row">
		<label for="inputEmail3" class="col-sm-2 col-form-label">ユーザー名</label>
		<div class="col-sm-10">
			<input name="user_name" type="text" class="form-control"
				value="${udb.name}" placeholder="ユーザー名">
		</div>
	</div>

	<!-- ログインID -->
	<div class="form-group row">
		<label for="inputEmail3" class="col-sm-2 col-form-label">ログインID</label>
		<div class="col-sm-10">
			<input name="login_id" type="text" class="form-control"
				value="${udb.loginId}" placeholder="ログインID">
		</div>
	</div>

	<!-- 住所 -->
	<div class="form-group row">
		<label for="inputEmail3" class="col-sm-2 col-form-label">住所</label>
		<div class="col-sm-10">
			<input name="login_id" type="text" class="form-control"
				value="${udb.address}" placeholder="住所">
		</div>
	</div>

	<p class="center-align">上記内容で登録しました。</p>
	<a href="Login"><button id="headbtn" class="btn btn-primary"
			type="submit">ログイン画面へ</button></a>

</body>
</html>