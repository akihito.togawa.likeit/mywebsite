<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>システムエラー</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<div class="full-width" id="header">
		<div class="row justify-content-md-center">
			<a href="Home"><div id="title" class="brand-logo">防音材ECサイト</div></a>
			<div id="right">
				<a href="MyUserData">マイページ</a>
			</div>

			<div id="right">
				<a href=MyCart>カート</a>
			</div>

			<div>
				<a href="MyLogout"><button id="headbtn" class="btn btn-primary"
						type="submit">ログアウト</button></a>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="section"></div>
			<div class="col s8 offset-s2">
				<div class="card grey lighten-5">
					<div class="card-content">
						<div class="row center">
							<h4 class="red-text">システムエラーが発生しました</h4>
							<h5 class="red-text">${errorMessage}</h5>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">
									<a href="Home"
										class="btn btn-large waves-effect waves-light  col s8 offset-s2">TOPページへ</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>