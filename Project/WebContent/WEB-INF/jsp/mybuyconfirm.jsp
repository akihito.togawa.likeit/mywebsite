<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト 購入確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<h1 class="row justify-content-md-center m-3">購入</h1>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th scope="col">商品名</th>
				<th scope="col">単価</th>
				<th scope="col">小計</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="cartInItem" items="${cart}">
				<tr>
					<td class="center">${cartInItem.name}</td>
					<td class="center">${cartInItem.formatPrice}円</td>
					<td class="center">${cartInItem.formatPrice}円</td>
				</tr>
			</c:forEach>
			<tr>
				<td class="center">${bdb.deliveryMethodName}</td>
				<td class="center"></td>
				<td class="center">${bdb.deliveryMethodPrice}円</td>
			</tr>
			<tr>
				<td class="center"></td>
				<td class="center">合計</td>
				<td class="center">${bdb.formatTotalPrice}円</td>
			</tr>
		</tbody>
	</table>
	<form action="MyBuyResult" method="post">
		<div class="row justify-content-md-center m-3">
			<button type="submit" class="btn btn-primary btn-lg">購入</button>
		</div>
	</form>
</body>
</html>