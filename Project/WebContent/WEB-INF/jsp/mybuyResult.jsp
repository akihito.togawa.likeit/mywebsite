<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト 購入結果</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<h2 class="row justify-content-md-center m-3">購入が完了しました</h2>

	<div class="row justify-content-md-center m-3">
		<div class="row">
			<div class="col">
				<a class="btn btn-primary" href="Home" role="button" style="white-space: nowrap">引き続き買い物をする</a>
			</div>
			<div class="col">
				<a class="btn btn-primary" href="MyUserData" role="button">ユーザー情報へ</a>
			</div>
		</div>
	</div>

	<h1 class="row justify-content-md-center m-3">購入詳細</h1>
	<!-- 購入日時 -->
	<table class="table table-bordered">
		<thead>
			<tr>
				<th scope="col">購入日時</th>
				<th scope="col">配送方法</th>
				<th scope="col">合計金額</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="center">${resultBDB.formatDate}</td>
				<td class="center">${resultBDB.deliveryMethodName}</td>
				<td class="center">${resultBDB.formatTotalPrice}円</td>
			</tr>
		</tbody>
	</table>

	<!-- 購入商品 -->
	<table class="table table-bordered">
		<thead>
			<tr>
				<th scope="col">商品名</th>
				<th scope="col">単価</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="buyIDB" items="${buyIDBList}">
				<tr>
					<td class="center">${buyIDB.name}</td>
					<td class="center">${buyIDB.formatPrice}円</td>
				</tr>
			</c:forEach>
			<tr>
				<td class="center">${resultBDB.deliveryMethodName}</td>
				<td class="center">${resultBDB.deliveryMethodPrice}円</td>
			</tr>
		</tbody>
	</table>
</body>
</html>