<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト ホーム画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<h2>
		商品検索結果
		</h1>
		<h3>検索結果◯件
	</h2>

	<!-- 商品一覧 -->
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="container">
					<div class="row">
						<div class="col">
							<img src="img/typeA.jpg" alt="一人静タイプA" title="typeA"
								width="300px" height="200px">
						</div>
						<div class="col">
							<p class="font-weight-bold">一人静 タイプA</p>
							<p>50,000円</p>
							<p style="color: #aca1a1">薄型・軽量の防音材</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="container">
					<div class="row">
						<div class="col">
							<img src="img/sample.png" alt="一人静タイプD" title="typeA"
								width="300px" height="200px">
						</div>
						<div class="col">
							<p class="font-weight-bold">一人静 タイプD</p>
							<p>60,000円</p>
							<p style="color: #aca1a1">薄型・軽量の防音材</p>
						</div>
					</div>
				</div>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<div class="container">
					<div class="row">
						<div class="col">
							<img src="img/sample.png" alt="一人静タイプD" title="typeA"
								width="300px" height="200px">
						</div>
						<div class="col">
							<p class="font-weight-bold">一人静 タイプD</p>
							<p>50,000円</p>
							<p style="color: #aca1a1">薄型・軽量の防音材</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="container">
					<div class="row">
						<div class="col">
							<img src="img/sample.png" alt="一人静タイプEsprit" title="typeA"
								width="300px" height="200px">
						</div>
						<div class="col">
							<p class="font-weight-bold">一人静 タイプEsprit</p>
							<p>3,980円</p>
							<p style="color: #aca1a1">薄型・軽量の防音材</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>