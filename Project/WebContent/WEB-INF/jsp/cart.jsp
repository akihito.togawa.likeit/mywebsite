<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト カート</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<div class="row justify-content-md-center m-3">
		${cartActionMessage}</div>

	<div class="row justify-content-md-center m-3">
		<h1>カート</h1>
	</div>


	<form action="MyItemDelete" method="POST">
		<div class="row justify-content-md-center m-3">
			<div class="row">
				<div class="col">
					<button type="submit" class="btn btn-primary btn-lg">削除</button>
				</div>
				<div class="col">
					<a href=MyBuy><button type="button"
							class="btn btn-primary btn-lg" style="white-space: nowrap">レジに進む</button></a>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center m-3">
			<c:forEach var="item" items="${cart}" varStatus="status">
				<div class="cart" style="width: 18rem;">
					<a href="Item?item_id=${item.id}"><img
						src="img/${item.fileName}" width="287px" height="200px"> </a>
					<div class="card-body">
						<div class="col">
							<p class="font-weight-bold">${item.name}</p>
							<p>${item.formatPrice}円</p>
							<div class="custom-control custom-checkbox">
								<input type="checkbox" id="${status.index}"
									name="delete_item_id_list" value="${item.id}" /> <label
									for="${status.index}">削除</label>
							</div>
						</div>
					</div>
				</div>
				<c:if test="${(status.index+1) % 4 == 0 }">
					<div class="row"></div>
				</c:if>
			</c:forEach>
		</div>
	</form>

</body>

</html>