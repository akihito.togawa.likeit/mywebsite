<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト 購入履歴詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<h3>購入履歴詳細</h3>

	<!-- 購入履歴詳細 -->
	<table class="table table-bordered">
		<thead>
			<tr>
				<th scope="col">購入日時</th>
				<th scope="col">配送方法</th>
				<th scope="col">合計金額</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="center">${buyDetailDate.formatDate}</td>
				<td class="center">${buyDetailDate.deliveryMethodName}</td>
				<td class="center">${buyDetailDate.formatTotalPrice}円</td>
			</tr>
		</tbody>
	</table>

	<!-- 商品一覧 -->
	<table class="table table-bordered">
		<thead>
			<tr>
				<th scope="col">商品名</th>
				<th scope="col">単価</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="buyItem" items="${buyDetailDate2}">
				<tr>
					<td class="center">${buyItem.name}</td>
					<td class="center">${buyItem.price}円</td>
				</tr>
			</c:forEach>
			<tr>
				<td class="center">${buyDetailDate.deliveryMethodName}</td>
				<td class="center">${buyDetailDate.deliveryMethodPrice}円</td>
			</tr>
		</tbody>
	</table>

</body>
</html>