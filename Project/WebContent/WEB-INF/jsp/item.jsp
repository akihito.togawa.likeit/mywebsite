<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト ホーム画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<div class="row justify-content-md-center m-3">
		<h3 class="">商品詳細</h3>
	</div>
	<div class="row justify-content-md-center m-3">
		<form action="MyItemAdd" method="POST">
			<input type="hidden" name="item_id" value="${item.id}">
			<button class="btn waves-effect waves-light bg-primary text-white" type="submit"
				name="action">買い物かごに追加</button>
		</form>
	</div>

	<div class="col">
		<div class="container">
			<div class="row">
				<div class="col">
					<a href="Item?item_id=${item.id}"><img
						src="img/${item.fileName}" width="780px" height="448px"></a>
				</div>
				<div class="col">
					<p class="font-weight-bold">${item.name}</p>
					<p>${item.formatPrice}円</p>
					<p>${item.detail}</p>
				</div>
			</div>
		</div>
	</div>

</body>
</html>