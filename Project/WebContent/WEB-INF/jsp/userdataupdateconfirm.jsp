<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト ユーザー情報確認画面</title>

<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<h5>入力内容確認</h5>
	<P class="red-text">${validationMessage}</P>

	<form action="MyUserDataUpdateResult" method="POST">
		<input type="hidden" name="id" value="${userDate.id}">

		<!-- ユーザー名 -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">ユーザー名</label>
			<div class="col-sm-10">
				<input name="user_name" type="text" class="form-control"
					value="${udb.name}" placeholder="ユーザー名">
			</div>
		</div>

		<!-- ログインID -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-10">
				<input name="login_id" type="text" class="form-control"
					value="${udb.loginId}" placeholder="ログインID">
			</div>
		</div>

		<!-- 住所 -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">住所</label>
			<div class="col-sm-10">
				<input name="address" type="text" class="form-control"
					value="${udb.address}" placeholder="住所">
			</div>
		</div>

		<p class="center-align">上記内容で更新してよろしいでしょうか?</p>

		<button type="submit" class="btn btn-primary mb-2" name="confirm_button" value="cancel">修正</button>
		<button type="submit" class="btn btn-primary mb-2" name="confirm_button" value="regist">更新</button>
	</form>
</body>
</html>