<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト ユーザー情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<h5>ユーザー情報更新</h5>
	<c:if test="${validationMessage != null}">
		<P class="red-text">${validationMessage}</P>
	</c:if>


	<form action="MyUserDataUpdateConfirm" method="post">
		<input type="hidden" name="id" value="${userDate.id}">

		<!-- ユーザー名 -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">ユーザー名</label>
			<div class="col-sm-10">
				<!-- <input name="user_name" type="text" class="form-control"
					id="inputEmail3" placeholder="ユーザー名"> -->
				<input type="text" name="user_name" value="${userDate.name}">
			</div>
		</div>

		<!-- ログインID -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-10">
				<input type="text" name="login_id" value="${userData.loginId}">
			</div>
		</div>

		<!-- 住所 -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">住所</label>
			<div class="col-sm-10">
				<input type="text" name="address" value="${userData.address}">
			</div>
		</div>

		<!-- パスワード -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">パスワード</label>
			<div class="col-sm-10">
				<input name="password" type="password" class="form-control"
					id="inputEmail3" placeholder="パスワード">
			</div>
		</div>

		<!-- パスワード確認用 -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">パスワード確認用</label>
			<div class="col-sm-10">
				<input name="confirm_password" type="password" class="form-control"
					id="inputEmail3" placeholder="パスワード確認用">
			</div>
		</div>

		<button type="submit" class="btn btn-primary mb-2">確認</button>
	</form>
</body>
</html>