<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト マイページ</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- 共通のcss -->
<link rel="stylesheet" href="css/common.css">
<!-- ログイン画面用のcss -->
<link rel=”stylesheet” type=”text/css” href=”css/common.css”>
<link rel=”stylesheet” type=”text/css” href=”project/css/login.css”>
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<h3 class="row justify-content-md-center m-3">マイページ</h3>
	<div class="row">
		<!-- ユーザー名出力 -->
		<div class="input-field col s6 text-right">
			<label>名前</label> <input type="text" name="user_name"
				value="${userData.name}">
		</div>
		<!-- ログインID出力 -->
		<div class="input-field col s6">
			<div class="form-group">
				<label>ログインID</label> <input type="text" name="user_name"
					value="${userData.loginId}">
			</div>
		</div>
	</div>

	<div class="row">
		<div class="input-field col s6 text-right">
			<label>住所</label> <input type="text" name="user_name"
				value="${userData.address}">
		</div>
		<div class="input-field col s6 text-right"></div>
	</div>
	<div class="row justify-content-md-center m-3">
		<a href="MyUserDataUpdate" type="submit" class="btn btn-primary mb-2">更新</a>
	</div>
	<!-- 購入履歴 -->
	<table class="table table-bordered">
		<thead>
			<tr>
				<th scope="col"></th>
				<th scope="col">購入日時</th>
				<th scope="col">配送方法</th>
				<th scope="col">合計金額</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="buyDetailData" items="${buyDataList}">
				<tr>
					<td class="center"><a
						href="MyUserBuyHistoryDetail?buy_id=${buyDetailData.id}"
						class="btn btn-primary">詳細</a></td>
					<td class="center">${buyDetailData.formatDate}</td>
					<td class="center">${buyDetailData.deliveryMethodName}</td>
					<td class="center">${buyDetailData.formatTotalPrice}円</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>