<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト ユーザ-情報更新</title>

<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<h5>更新完了</h5>
	<P class="red-text">${validationMessage}</P>

	<form action="MyUserDataUpdateResult" method="POST">
		<!-- ユーザー名 -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">ユーザー名</label>
			<div class="col-sm-10">
				<input name="user_name" type="text" class="form-control"
					value="${udb.name}" placeholder="ユーザー名">
			</div>
		</div>

		<!-- ログインID -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-10">
				<input name="login_id" type="text" class="form-control"
					value="${udb.loginId}" placeholder="ログインID">
			</div>
		</div>

		<p class="center-align">上記内容で更新しました。</p>
		<a href="MyUserData">ユーザー情報へ</a>
	</form>
</body>
</html>