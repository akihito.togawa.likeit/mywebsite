<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト 新規ユーザー登録</title>

<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<div class="full-width" id="header">
		<div class="row justify-content-md-center">
			<div id="title" class="col">防音材ECサイト</div>
		</div>
	</div>

	<h5>入力内容確認</h5>
	<P class="red-text">${validationMessage}</P>

	<form action="MyRegistResult" method="POST">
		<!-- ユーザー名 -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">ユーザー名</label>
			<input type="text" name="user_name" value="${udb.name}" >
		</div>

		<!-- ログインID -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">ログインID</label>
			<input type="text" name="login_id" value="${udb.loginId}" >
		</div>

		<!-- 住所 -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">住所</label>
			<input type="text" name="address" value="${udb.address}" >
		</div>

		<!-- パスワード -->
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 col-form-label">パスワード</label>
			<input type="password" name="password" value="${udb.password}" >

		</div>


		<p class="center-align">上記内容で登録してよろしいでしょうか?</p>

		<button type="submit" class="btn btn-primary mb-2"
			name="confirm_button" value="cancel">修正</button>
		<button type="submit" class="btn btn-primary mb-2"
			name="confirm_button" value="regist">登録</button>
	</form>
</body>
</html>