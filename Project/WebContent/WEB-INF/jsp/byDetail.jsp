<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>防音材ECサイト 購入詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />


	<h1>購入詳細</h1>

	<!-- 購入詳細１ -->
	<table class="table">
		<thead>
			<tr>
				<th scope="col">購入日時</th>
				<th scope="col">配送方法</th>
				<th scope="col">購入金額</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>0000年00月00日</td>
				<td>特急配送</td>
				<td>2000円</td>
			</tr>
		</tbody>
	</table>

	<!-- 購入詳細１ -->
	<table class="table">
		<thead>
			<tr>
				<th scope="col">商品名</th>
				<th scope="col">単価</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>商品A</td>
				<td>2000円</td>
			</tr>
		</tbody>
	</table>
</body>
</html>