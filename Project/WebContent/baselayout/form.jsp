<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<nav class="red darken-4" role="navigation">
	<div class="row justify-content-md-center">
		<form action="MyItemSearchResult" class="form-inline m-3">
			<select class="custom-select" name="search_category">
				<option selected value="全てのカテゴリー">全てのカテゴリー</option>
				<option value="高音">高音</option>
				<option value="低音">低音</option>
				<option value="振動">振動</option>
			</select> <label class="sr-only" for="inlineFormInputGroupUsername2">商品名</label>
			<div class="input-group mb-16 mr-md-16">
				<input type="text" name="search_word" class="form-control"
					id="inlineFormInputGroupUsername2" placeholder="商品名">
			</div>

			<button type="submit" class="btn btn-primary ml-2">商品を探す</button>
		</form>
	</div>
</nav>