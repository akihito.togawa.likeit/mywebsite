<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<nav class="red darken-4" role="navigation">
	<!-- ヘッダー -->
	<div id="header">
		<div class="row justify-content-md-center">
			<a href="Home"><div id="title" class="brand-logo">防音材ECサイト</div></a>
			<input name="prodId" type="hidden" value="userId"> <input
				name="prodId" type="hidden" value="loginId">

			<c:choose>
				<c:when test="${userId != null}">
					<div class="row">
						<div id="right">
							<a href="MyUserData">マイページ</a>
						</div>

						<div id="right">
							<a href=MyCart>カート</a>
						</div>

						<div>
							<a href="MyLogout" id="headbtn" class="btn btn-primary">ログアウト</a>
						</div>
					</div>
				</c:when>

				<c:otherwise>
					<div class="row">
						<div id="right">
							<a href=MyCart>カート</a>
						</div>
						<div class="col-xs-4">
							<a href="Login" class="btn btn-primary m-1">ログイン</a>
						</div>
						<div class="col-xs-4">
							<a href="Regist" class="btn btn-primary m-1">新規登録</a>
						</div>
					</div>
				</c:otherwise>
			</c:choose>

			<%-- int userId
			=(int) session.getAttribute("userId");
			<%=if(userId != null){ %>
			<div class="row">
				<div id="right">
					<a href="MyUserData">マイページ</a>
				</div>

				<div id="right">
					<a href=MyCart>カート</a>
				</div>

				<div>
					<a href="MyLogout"><button id="headbtn" class="btn btn-primary"
							type="submit">ログアウト</button></a>
				</div>
			</div>
			<%}else{ %>
			<div class="row">
				<div id="right">
					<a href=MyCart>カート</a>
				</div>
				<div class="col-xs-4 bg-info">
					<button id="headbtn" type="submit" class="btn btn-primary mb-2">
						<a href="Login">ログイン</a>
					</button>
				</div>
				<div class="col-xs-4 bg-info">
					<button id="headbtn" type="submit" class="btn btn-primary mb-2">
						<a href="Regist">新規登録</a>
					</button>
				</div>
			</div>
			<%} %> --%>
		</div>
	</div>
</nav>