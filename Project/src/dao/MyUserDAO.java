package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;

public class MyUserDAO {
	// インスタンスオブジェクトを返却させてコードの簡略化

	/**
	 * データの挿入処理を行う。現在時刻は挿入直前に生成
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */

	public static boolean isOverlapLoginId(String loginId, int i) {
		return false;
	}

	//パスワード暗号化
	public static String password(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		return result;
	}

	//新規ユーザー登録
	public static void insertUser(UserDataBeans udb) throws SQLException {
		Connection con = null;
		try {
			con = DBManager.getConnection();
			//SELECT文を準備　
			String sql = "INSERT INTO t_user(name,login_id,address,login_password,create_date) VALUES(?,?,?,?, now())";
			//SELECT文を実行
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, udb.getName());
			pStmt.setString(2, udb.getLoginId());
			pStmt.setString(3, udb.getAddress());
			pStmt.setString(4, udb.getPassword());
			pStmt.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//ユーザーID取得
	public static int getUserId(String loginId, String password) throws SQLException {
		Connection con = null;
		//PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE login_id =? and login_password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			int userId = 0;

			while (rs.next()) {
				if (password.equals(rs.getString("login_password"))) {
					userId = rs.getInt("id");
					System.out.println("login succeeded");
					break;
				}
			}

			return userId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//ユーザー情報詳細参照
	public UserDataBeans detail(int userId) throws SQLException {
		Connection con = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE id=?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, userId);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			if (!rs.next()) {
				return null;
			}
			int Id = rs.getInt("id");
			String name = rs.getString("name");
			String loginId = rs.getString("login_id");
			String password = rs.getString("login_password");
			String createDate = rs.getString("create_date");
			String address = rs.getString("address");
			return new UserDataBeans(Id, name, loginId, password, createDate,address);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//ユーザー情報更新
	public void userDateUpdate(String userId, String updateName, String updateLoginId, String updateaddress,
			String updatePassword) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// UPDATE文を準備
			String sql = "UPDATE t_user SET name =?, login_id = ?,address = ? login_password = ? WHERE id = ?";

			// UPDATEを実行し、結果表を取得
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, updateName);
			pStmt.setString(2, updateLoginId);
			pStmt.setString(3, updateaddress);
			pStmt.setString(4, updatePassword);
			pStmt.setString(5, userId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}