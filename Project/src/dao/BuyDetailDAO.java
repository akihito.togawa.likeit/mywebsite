package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.MyItemDataBeans;

/**
 *
 * @author d-yamaguchi
 *
 */
public class BuyDetailDAO {

	/**
	 * 購入詳細登録処理
	 * @param bddb BuyDetailDataBeans
	 * @throws SQLException
	 * 			呼び出し元にスローさせるため
	 */
	public static void insertBuyDetail(BuyDetailDataBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy_detail(buy_id,item_id) VALUES(?,?)");
			st.setInt(1, bddb.getBuyId());
			st.setInt(2, bddb.getItemId());
			st.executeUpdate();
			System.out.println("inserting BuyDetail has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入情報検索
	 * @param buyId
	 * @return {BuyDataDetailBeans}
	 * @throws SQLException
	 */
	public ArrayList<BuyDetailDataBeans> getBuyDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_buy_detail WHERE buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<BuyDetailDataBeans> buyDetailList = new ArrayList<BuyDetailDataBeans>();

			while (rs.next()) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setId(rs.getInt("id"));
				bddb.setBuyId(rs.getInt("buy_id"));
				bddb.setItemId(rs.getInt("item_id"));
				buyDetailList.add(bddb);
			}

			System.out.println("searching BuyDataBeansList by BuyID has been completed");
			return buyDetailList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	 /**
     * 購入IDによる購入詳細情報検索
     * @param buyId
     * @return buyDetailItemList ArrayList<ItemDataBeans>
     *             購入詳細情報のデータを持つJavaBeansのリスト
     * @throws SQLException
     */
	public static ArrayList<MyItemDataBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT m_item.id,"
					+ " m_item.name,"
					+ " m_item.price"
					+ " FROM t_buy_detail"
					+ " JOIN m_item"
					+ " ON t_buy_detail.item_id = m_item.id"
					+ " WHERE t_buy_detail.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<MyItemDataBeans> buyDetailItemList = new ArrayList<MyItemDataBeans>();

			while (rs.next()) {
				MyItemDataBeans idb = new MyItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));


				buyDetailItemList.add(idb);
			}

			System.out.println("searching ItemDataBeansList by BuyID has been completed");
			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

//	課題２−１
	public static ArrayList<BuyDataBeans> getBuyDataDetailBeansByUserId(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM t_buy"
							+ " JOIN m_delivery_method"
							+ " ON t_buy.delivery_method_id = m_delivery_method.id"
							+ " WHERE t_buy.user_id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();


			ArrayList<BuyDataBeans> buyDateList = new ArrayList<BuyDataBeans>();
			while (rs.next()) {
				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setId(rs.getInt("id"));
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setDelivertMethodId(rs.getInt("delivery_method_id"));
				bdb.setBuyDate(rs.getTimestamp("create_date"));
				bdb.setDeliveryMethodName(rs.getString("name"));
				bdb.setDeliveryMethodPrice(rs.getInt("price"));
				buyDateList.add(bdb);
			}

			System.out.println("searching BuyDataBeans by buyID has been completed");

			return buyDateList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


//	課題２−２ 配送方法など
	public static BuyDataBeans getBuyDataByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM t_buy_detail "
					+"JOIN t_buy ON t_buy_detail.buy_id = t_buy.id "
					+"JOIN m_delivery_method ON t_buy.delivery_method_id = m_delivery_method.id "
					+"WHERE t_buy.id = ? "
					+"AND t_buy.delivery_method_id = m_delivery_method.id");

			st.setInt(1, buyId);
			ResultSet rs = st.executeQuery();

			 if (!rs.next()) {
	                return null;
	            }
				Timestamp createDate = rs.getTimestamp("create_date");
				String name = rs.getString("name");
				int toralpraice = rs.getInt("total_price");
				String DeliveryMethodName =rs.getString("name");
				int DeliveryMethodPrice =rs.getInt("price");

	            return new  BuyDataBeans(createDate,name,toralpraice,DeliveryMethodName,DeliveryMethodPrice);

//			System.out.println("searching BuyDataBeans by buyID has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


//	課題２−２ 商品名
	public static ArrayList<MyItemDataBeans> getItemDataBeansList(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM t_buy_detail " //←エラーを見てSQL文が繋がっていたら最後にスペースを入れる
					+"JOIN m_item ON t_buy_detail.item_id = m_item.id "
//					+"JOIN m_delivery_method ON t_buy.delivery_method_id = m_delivery_method.id  "
					+"WHERE t_buy_detail.buy_id = ?");



			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<MyItemDataBeans> buyDetailList2 = new ArrayList<MyItemDataBeans>();

			while (rs.next()) {
				MyItemDataBeans bddb = new MyItemDataBeans();
				bddb.setId(rs.getInt("id"));
				bddb.setName(rs.getString("name"));
				bddb.setPrice(rs.getInt("price"));
				buyDetailList2.add(bddb);
			}

			System.out.println("searching BuyDataBeansList by BuyID has been completed");
			return buyDetailList2;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
		}
	}
	}


}
