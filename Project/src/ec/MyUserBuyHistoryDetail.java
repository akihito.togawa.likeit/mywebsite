package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.MyItemDataBeans;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class MyUserBuyHistoryDetail
 */
@WebServlet("/MyUserBuyHistoryDetail")
public class MyUserBuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション開始
		HttpSession session = request.getSession();
		try {
			//					int buyId = request.getParameter("buy_id");
			int buyId = Integer.parseInt(request.getParameter("buy_id"));

			System.out.println(buyId);

			//配送方法など
			BuyDataBeans buyDetailDate = BuyDetailDAO.getBuyDataByBuyId(buyId);
			request.setAttribute("buyDetailDate", buyDetailDate);

			//商品名
			ArrayList<MyItemDataBeans> buyDetailDate2 = BuyDetailDAO.getItemDataBeansList(buyId);
			request.setAttribute("buyDetailDate2", buyDetailDate2);

			request.getRequestDispatcher(EcHelper.USER_BUY_HISTORY_DETAIL_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
