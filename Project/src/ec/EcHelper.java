package ec;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import beans.MyItemDataBeans;

/**
 * Servlet implementation class EcHelper
 */
@WebServlet("/EcHelper")

public class EcHelper {
	// ログイン
	static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
	// ログアウト
	static final String LOGOUT_PAGE = "/WEB-INF/jsp/mylogout.jsp";
	// 新規登録
	static final String REGIST_PAGE = "/WEB-INF/jsp/regist.jsp";
	// 新規登録入力内容確認
	static final String REGIST_CONFIRM_PAGE = "/WEB-INF/jsp/registconfirm.jsp";
	// 新規登録完了
	static final String REGIST_RESULT_PAGE = "/WEB-INF/jsp/registresult.jsp";
	// エラーページ
	static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
	// TOPページ
	static final String TOP_PAGE = "/WEB-INF/jsp/home.jsp";
	// ユーザー情報
	static final String USER_DATA_PAGE = "/WEB-INF/jsp/userData.jsp";
	// ユーザー情報更新
	static final String USER_DATA_UPDATE_PAGE = "/WEB-INF/jsp/userdataupdate.jsp";
	// ユーザー情報更新確認
	static final String USER_DATA_UPDATE_CONFIRM_PAGE = "/WEB-INF/jsp/userdataupdateconfirm.jsp";
	// ユーザー情報更新完了
	static final String USER_DATA_UPDATA_RESULT_PAGE = "/WEB-INF/jsp/userdataupdateresult.jsp";
	// 商品ページ
	static final String ITEM_PAGE = "/WEB-INF/jsp/item.jsp";
	// 買い物かごページ
	static final String CART_PAGE = "/WEB-INF/jsp/cart.jsp";
	// 購入
	static final String BUY_PAGE = "/WEB-INF/jsp/mybuy.jsp";
	// 購入確認
	static final String BUY_CONFIRM_PAGE = "/WEB-INF/jsp/mybuyconfirm.jsp";
	// 購入完了
	static final String BUY_RESULT_PAGE = "/WEB-INF/jsp/mybuyResult.jsp";
	// ユーザー購入履歴
	static final String USER_BUY_HISTORY_DETAIL_PAGE = "/WEB-INF/jsp/myuserbuyhistorydetail.jsp";
	// 検索結果
	static final String SEARCH_RESULT_PAGE = "/WEB-INF/jsp/myitemsearchresult.jsp";

	public static EcHelper getInstance() {
		return new EcHelper();
	}

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 *
	 * @param session
	 * @param str
	 * @return
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}

	/**
	 * ログインIDのバリデーション
	 *
	 * @param inputLoginId
	 * @return
	 */
	public static boolean isLoginIdValidation(String inputLoginId) {
		// 英数字アンダースコア以外が入力されていたら
		if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
			return true;
		}

		return false;

	}

	public static Object getSha256(String password) {
		MessageDigest md = null;
		StringBuffer buf = new StringBuffer();
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(password.getBytes());
			byte[] digest = md.digest();

			for (int i = 0; i < digest.length; i++) {
				buf.append(String.format("%02x", digest[i]));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return buf.toString();
	}

	public static int getTotalItemPrice(ArrayList<MyItemDataBeans> items) {
		int total = 0;
		for (MyItemDataBeans item : items) {
			total += item.getPrice();

		}
		//		total +=
		return total;
	}

}
