package ec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;

/**
 * Servlet implementation class MyUserDataUpdateResult
 */
@WebServlet("/MyUserDataUpdateResult")
public class MyUserDataUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//セッション開始
		HttpSession session = request.getSession();

		try {
			String updateUserName = request.getParameter("user_name");
			String updateLoginId = request.getParameter("login_id");
			String updatePassword = request.getParameter("password");
			UserDataBeans udb = new UserDataBeans();
			udb.setName(updateUserName);
			udb.setLoginId(updateLoginId );
			udb.setPassword(updatePassword);

			// 登録が確定されたかどうか確認するための変数
			String confirmed = request.getParameter("confirm_button");
			switch (confirmed) {
			case "cancel":
				session.setAttribute("udb", udb);
				response.sendRedirect("MyUserDataUpdate");
				break;

			case "regist":
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(EcHelper.USER_DATA_UPDATA_RESULT_PAGE).forward(request, response);
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
