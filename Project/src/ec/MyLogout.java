package ec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class logout
 */
@WebServlet("/MyLogout")
public class MyLogout extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		// ログイン時に保存したセッション内のユーザ情報を削除
		session.removeAttribute("login_id");

		session.setAttribute("isLogin", false);
		session.removeAttribute("userId");

		// ログインのサーブレットにリダイレクト
		request.getRequestDispatcher(EcHelper.LOGOUT_PAGE).forward(request, response);
	}

}
