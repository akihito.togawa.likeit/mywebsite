package ec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MyUserDAO;

/**
 * Servlet implementation class LoginResult
 */
@WebServlet("/MyLoginResult")
public class MyLoginResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {
			String loginId = request.getParameter("login_id");
			String password = request.getParameter("password");

			int userId = MyUserDAO.getUserId(loginId, MyUserDAO.password(password));

			//ユーザーIDが取得できたなら
			if (userId != 0) {
				session.setAttribute("userId", userId);
				session.setAttribute("loginId", loginId);
				response.sendRedirect("Home");
			} else {
				session.setAttribute("userId", userId);
				session.setAttribute("loginId", loginId);
				session.setAttribute("loginErrorMessage", "入力内容が正しくありません");
				response.sendRedirect("Login");
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
