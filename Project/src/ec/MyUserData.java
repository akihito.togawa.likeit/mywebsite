package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDetailDAO;
import dao.MyUserDAO;

/**
 * Servlet implementation class UserData
 */
@WebServlet("/MyUserData")
public class MyUserData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション開始
				HttpSession session = request.getSession();

		try {
			// ログイン時に取得したユーザーIDをセッションから取得
			int userId = (int) session.getAttribute("userId");

			// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する
			String validationMessage = (String) EcHelper.cutSessionAttribute(session, "validationMessage");

			request.setAttribute("validationMessage", validationMessage);

			// ユーザ一覧情報を取得
//			BuyDetailDAO userDao = new BuyDetailDAO();
			MyUserDAO userDao =new MyUserDAO();
			UserDataBeans userData =userDao.detail(userId);

			// リクエストスコープにユーザ一覧情報をセット
			session.setAttribute("userData", userData);

			// ユーザ一覧情報を取得
			ArrayList<BuyDataBeans> buyDataList = BuyDetailDAO.getBuyDataDetailBeansByUserId(userId);

			// リクエストスコープにユーザ一覧情報をセット
			session.setAttribute("buyDataList", buyDataList);

			request.getRequestDispatcher(EcHelper.USER_DATA_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
