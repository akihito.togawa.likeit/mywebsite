package ec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.MyUserDAO;

/**
 * Servlet implementation class MyUserDataUpdateConfirm
 */
@WebServlet("/MyUserDataUpdateConfirm")
public class MyUserDataUpdateConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//セッション開始
		HttpSession session = request.getSession();

		try {
			// ログイン時に取得したユーザーIDをセッションから取得
			String userId = request.getParameter("id");

			String updateUserName = request.getParameter("user_name");
			String updateLogintId = request.getParameter("login_id");
			String updateaddress = request.getParameter("address");
			String updatePassword = request.getParameter("password");
			String updateConfirmPassword = request.getParameter("confirm_password");

			MyUserDAO userDAO = new MyUserDAO();
			userDAO.userDateUpdate(userId, updateUserName, updateLogintId,updateaddress, updatePassword);

			UserDataBeans udb = new UserDataBeans();
			udb.setName(updateUserName);
			udb.setLoginId(updateLogintId);
			udb.setAddress(updateaddress);
			udb.setPassword(updatePassword);

			//エラーメッセージを格納する変数
			String validationMessage = "";

			//入力されたパスワードと確認用パスワードが正しいか
			if (!updatePassword.equals(updateConfirmPassword)) {
				validationMessage += "入力されているパスワードと確認用パスワードが違います<br>";
			}

			// ログインIDの入力規則チェック 英数字 ハイフン アンダースコアのみ入力可能
			if (!EcHelper.isLoginIdValidation(udb.getLoginId())) {
				validationMessage += "半角英数とハイフン、アンダースコアのみ入力できます";
			}

			//			// loginIdの重複をチェック
			//			if (UserDAO.isOverlapLoginId(udb.getLoginId(), 0)) {
			//				validationMessage += "ほかのユーザーが使用中のログインIDです";
			//			}

			//バリデーションエラーメッセージがないなら確認画面へ
			if (validationMessage.length() == 0) {
				//確認画面へ
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(EcHelper.USER_DATA_UPDATE_CONFIRM_PAGE).forward(request, response);
			} else {
				//セッションにエラーメッセージを持たせてユーザー画面へ
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("UserData");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
