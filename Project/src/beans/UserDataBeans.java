package beans;

public class UserDataBeans {
	private int id;
	private String name;
	private String loginId;
	private String password;
	private String createDate;
	private String address;

	public UserDataBeans() {
		this.id=0;
		this.name="";
		this.loginId="";
		this.password="";
	}

	public UserDataBeans(String loginIdData, String nameData) {

	}

	public UserDataBeans(int id, String name, String loginId, String password, String createDate,String address) {
		this.id=id;
		this.name=name;
		this.loginId=loginId;
		this.password=password;
		this.createDate=createDate;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password =password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public void setUpdateUserDataBeansInfo( int id,String name, String loginId, String address,String password) {
		this.id=id;
		this.name=name;
		this.loginId=loginId;
		this.address = address;
		this.password=password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
